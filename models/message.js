var mongoose = require("mongoose");

var messageSchema = new mongoose.Schema({
    firstName: String,
    lastName: String,
    email: String,
    message: String,
    action: { type: String, default: "pending" },
    createdAt: { type: Date, default: Date.now }
});

module.exports = mongoose.model("Message", messageSchema);
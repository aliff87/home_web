/* global $ */

$(function(){
    $(".slideDownItem").slideDown("slow");
});

$(document).ready(function() {
    
    $("#giantHeaderCon").fadeIn("slow");
    
    
    var links = $('.navbar-nav a');
    $.each(links, function (key, val) {
        if (val.href == document.URL) {
            $(this).addClass('active');
        }
    });
    
    // Get the modal
    var modal = document.getElementById('myModal');
    var bckdrp = document.getElementById('backdrop');
    var dismissModal = function() {
        modal.style.display = "none";
        bckdrp.style.display = "none";
    };
    
    bckdrp.onclick = dismissModal;
    var span = document.getElementsByClassName("close")[0];
    span.onclick = dismissModal;
    $("#promoBtn").fadeIn(3000);

    $('#fixed-menu').hide();
    // fix menu when passed
    $('#particles-js').visibility({
        once: false,
        onBottomPassed: function() {
            $('#fixed-menu').fadeIn("slow");
            $('#topBtn').fadeIn("fast");
        },
        onBottomPassedReverse: function() {
            $('#fixed-menu').fadeOut("slow");
            $('#topBtn').fadeOut("slow");
        }
    });
    // slide down
    $('#descSection1').visibility({
        once: true,
        onTopPassed: function() {
            $(".landing-jumbotron").slideDown("slow");
        }
    });
    
    // Lazy load image
    $('#giantHeaderCon').visibility({
        once: true,
        onTopPassed: function() {
            $(".bgImg").show();
        }
    });
    
    // up Button
    $("#topBtn").click(function(){
        $("html, body").animate({ scrollTop: 0 }, 500);
            return false;
    });
    
     $('#step-item').visibility({
        once: true,
        onBottomVisible: function() {
            $("#step1").fadeIn(500);
            $("#step2").delay(500).fadeIn(500);
            $("#step3").delay(1000).fadeIn(500);
            
        }
    });
    
    $('#portfolioCard-container').visibility({
        once: true,
        onBottomVisible: function() {
            $("#portfolio-card").slideLeft(400);
        }
    });
    
    $("#blog-1").hover(function() {
      $("#slide1").slideDown("fast");
      $("#overlay-1").fadeIn("fast");
    }, function() {
      $("#slide1").slideUp("slow");
      $("#overlay-1").fadeOut("fast");
    });
    $("#blog-2").hover(function() {
      $("#slide2").slideDown("fast");
      $("#overlay-2").fadeIn("fast");
    }, function() {
      $("#slide2").slideUp("slow");
      $("#overlay-2").fadeOut("fast");
    });
    $("#blog-3").hover(function() {
      $("#slide3").slideDown("fast");
      $("#overlay-3").fadeIn("fast");
    }, function() {
      $("#slide3").slideUp("slow");
      $("#overlay-3").fadeOut("fast");
    });
    
    $("#collapseToggler").click(function() {
        $("#navbarNavAltMarkup").toggleClass("changeBg");
    });
});

// if (window.addEventListener) window.addEventListener('DOMMouseScroll', wheel, false);
// window.onmousewheel = document.onmousewheel = wheel;

// function wheel(event) {
//     var delta = 0;
//     if (event.wheelDelta) delta = event.wheelDelta / 50;
//     else if (event.detail) delta = -event.detail / 5;

//     handle(delta);
//     if (event.preventDefault) event.preventDefault();
//     event.returnValue = false;
// }

// var goUp = true;
// var end = null;
// var interval = null;

// function handle(delta) {
// 	var animationInterval = 10; //lower is faster
//   var scrollSpeed = 10; //lower is faster

// 	if (end == null) {
//   	end = $(window).scrollTop();
//   }
//   end -= 20 * delta;
//   goUp = delta > 0;

//   if (interval == null) {
//     interval = setInterval(function () {
//       var scrollTop = $(window).scrollTop();
//       var step = Math.round((end - scrollTop) / scrollSpeed);
//       if (scrollTop <= 0 || 
//           scrollTop >= $(window).prop("scrollHeight") - $(window).height() ||
//           goUp && step > -1 || 
//           !goUp && step < 1 ) {
//         clearInterval(interval);
//         interval = null;
//         end = null;
//       }
//       $(window).scrollTop(scrollTop + step );
//     }, animationInterval);
//   }
// }
//Module config
var express = require("express");
var passport = require("passport");
var router = express.Router();
var middlewareObj = require("../middleware");
var request = require("request");

// Model config
var Enquiry = require("../models/enquiry");
var User = require("../models/user");
var Message = require("../models/message");

router.get("/", function(req, res) {
    res.render("landing", {type: "", code: ""});
});

router.get("/contact", function(req, res) {
    res.render("contact");
});

router.get("/packages", function(req, res) {
    res.render("packages");
});

router.get("/portfolio", function(req, res) {
    res.render("portfolio");
});

router.get("/about", function(req, res) {
    res.render("about");
});

router.get("/test", function(req, res) {
    res.render("test", {type: "easy", code: "Make me a super awesome website"});
});

router.get("/admin", function(req, res) {
    res.render("adminLogin");
});

router.get("/registeradmin", function(req, res) {
    res.render("regAdmin");
});

router.get("/quotation/:packages", function(req, res) {
    var packages = req.params.packages;
    res.render("enquiryPage", {type: packages, code: ""});
});

router.post("/login", function(req, res, next) {
  passport.authenticate("local", function(err, user, info) {
    if (err) { return next(err); }
    if (!user) { req.flash("fault", "Wrong password or username."); return res.redirect("/admin"); }
    req.logIn(user, function(err) {
      if (err) { return next(err); }
      req.flash("success", "Welcome back "+ user.username + "!");
      return res.redirect("/enquiry");
    });
  })(req, res, next);
});

router.post("/registeradmin", function(req, res, next) {
    if(req.body.pin == process.env.ADMINPIN) {
        var newUser = {
            username: req.body.name,
            email: req.body.email,
            isAdmin: true
        };
        User.register(newUser, req.body.password, function(err, user) {
            if(err || !user) {
                req.flash("fault", err.message);
                res.redirect("/registeradmin");
            } else {
                    passport.authenticate("local", function(err, user, info) {
                    if (err) { return next(err); }
                    if (!user) { return res.redirect("/registeradmin"); }
                    req.logIn(user, function(err) {
                      if (err) { return next(err); }
                      req.flash("success", "Successfully registered your account!");
                      return res.redirect("/enquiry");
                    });
                  })(req, res, next);
            }
        });
    } else {
        req.flash("fault", "Pin is incorrect.");
        res.redirect("/registeradmin");
    }
});

router.get("/enquiry",middlewareObj.isAdmin, function(req, res) {
        Enquiry.find({}, function(err, enquiries) {
            if(err || !enquiries) {
                req.flash("fault", "Something went wrong.");
                res.redirect("back");
            } else {
                Message.find({}, function(err, messages) {
                if(err || !messages) {
                    req.flash("fault", "Something went wrong.");
                    res.redirect("back");
                } else {
                    res.render("enquiry", {messages: messages, enquiries: enquiries});
                }
            });
            }
        });
});

router.post("/enquiry", function(req, res) {
    if(req.body['g-recaptcha-response'] === undefined || req.body['g-recaptcha-response'] === '' || req.body['g-recaptcha-response'] === null) {
        req.flash("fault", "Please select captcha");
        res.redirect("/");
        return;
    // return res.json({"responseCode" : 1,"responseDesc" : "Please select captcha"});
  }
  // Put your secret key here.
  var secretKey = process.env.RECAPTSECRET;
  // req.connection.remoteAddress will provide IP address of connected user.
  var verificationUrl = "https://www.google.com/recaptcha/api/siteverify?secret=" + secretKey + "&response=" + req.body['g-recaptcha-response'] + "&remoteip=" + req.connection.remoteAddress;
  // Hitting GET request to the URL, Google will respond with success or error scenario.
  request(verificationUrl,function(error,response,body) {
    body = JSON.parse(body);
    // Success will be true or false depending upon captcha validation.
    if(body.success !== undefined && !body.success) {
      return res.json({"responseCode" : 1,"responseDesc" : "Failed captcha verification"});
    }
    // res.json({"responseCode" : 0,"responseDesc" : "Sucess"});
    Enquiry.create(req.body.enquiry, function(err, enquiry) {
        if(err || !enquiry) {
            console.log(err);
            req.flash("fault", "Something went wrong.");
            res.redirect("back");
        } else {
            middlewareObj.sendEmailNotMiddle();
            req.flash("success", "Enquiry posted.  We will get back to you shortly.  Meanwhile check out our packages.");
            res.redirect("/packages");
        }
    });
  });
  
    
});

router.post("/message", function(req, res, next) {
    Message.create(req.body.feedback, function(err, message) {
        if(err) {
            console.log(err);
            req.flash("fault", "Something went wrong.");
            res.redirect("back");
        } else {
            req.flash("success", "Message posted.  Thank you for your feedback.");
            res.redirect("/");
        }
    });
});

router.post("/:id/:action", function(req, res) {
    Enquiry.findByIdAndUpdate(req.params.id, req.params.action, function(err, enquiry) {
        if(err || !enquiry) {
            console.log(err);
            req.flash("fault", "Something went wrong.");
            res.redirect("back");
        } else {
            res.redirect("/enquiry");
        }
    });
});

router.delete("/:id/del", function(req, res) {
    Enquiry.findByIdAndRemove(req.params.id, function(err) {
        if(err) {
            console.log(err);
            req.flash("fault", "Something went wrong.");
            res.redirect("back");
        } else {
            res.redirect("/enquiry");
        }
    });
});

router.post("/:id/:action", function(req, res) {
    Message.findByIdAndUpdate(req.params.id, req.params.action, function(err, message) {
        if(err || !message) {
            console.log(err);
            req.flash("fault", "Something went wrong.");
            res.redirect("back");
        } else {
            res.redirect("/enquiry");
        }
    });
});

router.delete("/:id/del", function(req, res) {
    Message.findByIdAndRemove(req.params.id, function(err) {
        if(err) {
            console.log(err);
            req.flash("fault", "Something went wrong.");
            res.redirect("back");
        } else {
            res.redirect("/enquiry");
        }
    });
});

router.delete("/enquiry/:id", function(req, res) {
    Enquiry.findByIdAndRemove(req.params.id, function(err) {
        if(err) {
            console.log(err);
            req.flash("fault", "Something went wrong.");
            res.redirect("back");
        } else {
            req.flash("success", "Enquiry deleted");
            res.redirect("/enquiry");
        }
    });
});

router.get('/:fileName', function (req, res, next) {

  var options = {
    root: __dirname,
    dotfiles: 'deny',
    headers: {
        'x-timestamp': Date.now(),
        'x-sent': true
    }
  };

  var fileName = req.params.fileName;
  res.sendFile(fileName, options, function (err) {
    if (err) {
      console.log("send err");
      res.redirect("/");
      next(err);
    } else {
      console.log('Sent:', fileName);
    }
  });

});

router.all('*', function(req, res) {
  res.redirect("/");
});

module.exports = router;
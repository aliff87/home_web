var mongoose = require("mongoose");
var localMongoose = require("passport-local-mongoose");

var userSchema = new mongoose.Schema({
    username: String,
    password: String,
    email: String,
    createdAt: { type: Date, default: Date.now },
    isAdmin: { type: Boolean, default: false }
});

userSchema.plugin(localMongoose);

module.exports = mongoose.model("User", userSchema);
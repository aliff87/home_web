var mailer = require("nodemailer");
var {google} = require("googleapis");
var OAuth2 = google.auth.OAuth2;

var middlewareObj={};

middlewareObj.isAdmin = function(req, res, next) {
  if(req.isAuthenticated()) {
      if(req.user.isAdmin == true ) {
      return next();
  } else {
    req.flash("fault", "You don`t have clearance!");
    res.redirect("/admin");
    }
  } else {
    req.flash("fault", "You are not sign in!");
    res.redirect("/admin");
  }
};

middlewareObj.sendEmail = function(req, res, next) {
  var oauth2Client = new OAuth2(
    "354645664553-lb4c1nbg3fef9km1cj1o3mrmk8nurm9c.apps.googleusercontent.com",
    "lGOUz9-LfBsBqoEeoMP2zUS5",
    "https://developers.google.com/oauthplayground"
    );
    
    oauth2Client.setCredentials( {
        refresh_token: process.env.GOOGLEREFRESHTOKEN
    });
    
    var accessToken = "";
    oauth2Client.refreshAccessToken(function(err, tokens) {
      if(err) {
        console.log(err);
      } else {
        accessToken = tokens.access_token;
      }
    });
    
    let smtpTransport = mailer.createTransport( {
        host: "smtp.gmail.com",
        port: 465,
        secure: true,
        auth: {
            type: "OAuth2",
            user: "appsdigadvs@gmail.com",
            clientId: process.env.GOOGLECLIENTID,
            clientSecret: process.env.GOOGLECLIENTSECRET,
            refreshToken: process.env.GOOGLEREFRESHTOKEN,
            accessToken: accessToken
        }
    });

var mailOptions = {
    from: "appsdigadvs@gmail.com",
    to: "aliff87@yahoo.com",
    subject: "Test Done",
    generateTextFromHtml: true,
    html: "<p>Hello</p>"
};

smtpTransport.sendMail(mailOptions, function(err, response) {
    if(err) {
        console.log(err);
    } else {
        console.log("send!"+response);
    }
    smtpTransport.close();
});
    next();
};

middlewareObj.sendEmailNotMiddle = function(req, res, next) {
  console.log("send email yall");
  var oauth2Client = new OAuth2(
    "354645664553-lb4c1nbg3fef9km1cj1o3mrmk8nurm9c.apps.googleusercontent.com",
    "lGOUz9-LfBsBqoEeoMP2zUS5",
    "https://developers.google.com/oauthplayground"
    );
    
    oauth2Client.setCredentials( {
        refresh_token: process.env.GOOGLEREFRESHTOKEN
    });
    
    var accessToken = "";
    oauth2Client.refreshAccessToken(function(err, tokens) {
      if(err) {
        console.log(err);
      } else {
        accessToken = tokens.access_token;
      }
    });
    
    let smtpTransport = mailer.createTransport( {
        host: "smtp.gmail.com",
        port: 465,
        secure: true,
        auth: {
            type: "OAuth2",
            user: "appsdigadvs@gmail.com",
            clientId: process.env.GOOGLECLIENTID,
            clientSecret: process.env.GOOGLECLIENTSECRET,
            refreshToken: process.env.GOOGLEREFRESHTOKEN,
            accessToken: accessToken
        }
    });

var mailOptions = {
    from: "appsdigadvs@gmail.com",
    to: "hello@rabbitworx.com",
    subject: "Enquiry Receive",
    generateTextFromHtml: true,
    html: "<p>Received new enquiry </p><a href='https://rabbitworx.com/admin'>Rabbitworx login</a>"
};

smtpTransport.sendMail(mailOptions, function(err, response) {
    if(err) {
        console.log(err);
    } else {
        console.log("send!"+response);
    }
    smtpTransport.close();
});
};

module.exports = middlewareObj;
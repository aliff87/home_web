//Module config
require('dotenv').config();
var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
var passport = require("passport");
var localStrategy = require("passport-local");
var expressSession = require("express-session");
var methodOverride = require("method-override");
// var cookieParser = require("cookie-parser");
var flash = require("connect-flash");
var serverStatic = require("serve-static");
//Routes config
var indexRoutes = require("./routes/index");
//Schema import
var User = require("./models/user");

// //Mongoose config
// mongoose.connect("mongodb://localhost/home_web_db");
mongoose.connect(process.env.DATABASEURL);

//App set
app.set("view engine", "ejs");

//App use
//expressSession must be use before passport session

app.use(flash());
// app.use(cookieParser("key"));
app.use(expressSession({
    secret: "key",
    resave: false,
    saveUninitialized: false,
    cookie : {
        expires: false,
    },
}));

app.use(passport.initialize());
app.use(passport.session());
app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride("_method"));
app.use(express.static(__dirname + "/public"));
app.use(serverStatic("views/"));

//passport use
passport.use(new localStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use(function(req, res, next) {
    res.locals.currentUser = req.user;
    res.locals.success = req.flash("success");
    res.locals.fault = req.flash("fault");
    res.locals.success2 = req.flash("success2");
    res.locals.fault2 = req.flash("fault2");
    next();
});

//Routes import


app.use(indexRoutes);

//Server
app.listen(process.env.PORT, function(err) {
// app.listen(3033, function(err) {
    if(err) {
        console.log(err.message);
    } else {
        console.log("Server is running");
    }
})
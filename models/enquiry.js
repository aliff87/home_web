var mongoose = require("mongoose");

var enquirySchema = new mongoose.Schema({
    name: String,
    email: String,
    phone: Number,
    category: String,
    budget: String,
    remarks: String,
    action: { type: String, default: "pending" },
    createdAt: { type: Date, default: Date.now }
});

module.exports = mongoose.model("Enquiry", enquirySchema);